﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class BattleCarShooting : MonoBehaviourPunCallbacks
{
    [Header("Health")]
    public float startHealth = 50;
    private bool isDead;
    private float health;
    public Image healthBar;
    public Transform laserSpawnPos;
    public GameObject spectatorCamera;
    public Camera carCam;

    [Header("Kill Info")]
    public int killCount;

    public enum RaiseEventsCode
    {
        EliminationEventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.EliminationEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nameOfEliminatedPlayer = (string)data[0];

            Debug.Log(nameOfEliminatedPlayer + " has been eliminated.");

            GameObject eliminationText = GameObject.Find("elimText");

            eliminationText.GetComponent<Text>().text = nameOfEliminatedPlayer + " has been eliminated";
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
        health = startHealth;
        healthBar.fillAmount = health/startHealth;
        spectatorCamera = GameObject.Find("Main Camera");
        carCam = transform.Find("Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(laserSpawnPos.position, laserSpawnPos.up * 200, Color.red);

        if(killCount >= PhotonNetwork.CurrentRoom.PlayerCount - 1)
        {
            StartCoroutine(ShowWinner());
        }
    }

    public void ShootLaser()
    {
        RaycastHit hit;
        Ray ray = new Ray(laserSpawnPos.position, laserSpawnPos.up);
        
        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log("Hit: " + hit.collider.gameObject.name);
        }

        if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
        {
            hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);

            if(hit.collider.gameObject.GetComponent<BattleCarShooting>().health <= 0 && !hit.collider.gameObject.GetComponent<BattleCarShooting>().isDead)
            {
                hit.collider.gameObject.GetComponent<BattleCarShooting>().isDead = true;
                photonView.RPC("AddKill", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health/startHealth;

        if(health <= 0)
        {
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            StartCoroutine(KillText());

            IEnumerator KillText()
            {
                GameObject killText = GameObject.Find("killFeed");
                killText.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
                yield return new WaitForSeconds(2f);
                killText.GetComponent<Text>().text = "";
            }           
        }
    }

    public void Die()
    {
        // Spectator Camera
        GetComponent<PlayerSetup>().camera.transform.parent = spectatorCamera.transform;
        carCam.transform.position = spectatorCamera.transform.position;
        carCam.transform.rotation = spectatorCamera.transform.rotation;

        // Disable controls
        GetComponent<VehicleMovement>().enabled = false;

        // Event data
        string nickName = photonView.Owner.NickName;

        object[] data = new object[] {nickName};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.EliminationEventCode, data, raiseEventOptions, sendOptions);

        // destroy option
        // Destroy(gameObject);

        photonView.RPC("ResetIsDead", RpcTarget.AllBuffered);
        
    }

    [PunRPC]
    public void ResetIsDead()
    {
        isDead = false;
    }

    [PunRPC]
    public void AddKill()
    {
        killCount++;
    }

    IEnumerator ShowWinner()
    {
        GameObject winText = GameObject.Find("winText");
        float winTime = 3.0f;

        while(winTime > 0)
        {
            yield return new WaitForSeconds(1f);
            winTime--;

            winText.GetComponent<Text>().text = photonView.Owner.NickName + " has won the game.";
        }
        winText.GetComponent<Text>().text = "";
    }
}
