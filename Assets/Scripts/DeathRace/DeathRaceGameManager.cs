﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceGameManager : MonoBehaviour
{
    public GameObject[] battleCarPrefabs;
    public Transform[] startingPositions;
    public Text timerText;
    public List<GameObject> playerList = new List<GameObject>();
    public int playerCount;

    public static DeathRaceGameManager instance = null;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else if(instance != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);    
    }
    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log("Player Selection Number: " + (int) playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber-1].position;
                PhotonNetwork.Instantiate(battleCarPrefabs[(int) playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }

            Debug.Log("Player count: " + PhotonNetwork.CurrentRoom.PlayerCount);
            playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
